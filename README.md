Welcome to dune-fiber-elasticity!

# What is this module?

This module provides an implementation for a PDELab `LocalOperator`
for fiber reinforced elasticity. The main header to include is
`dune/fiber-elasticity/operator.hh` and the main class to use is
`FibreReinforcedBulkOperator` from that header.

This module is C&Ped from several other modules, hence its overall
code structure is a bit weird.

# Prerequisites

A working dune-pdelab stack including dune-uggrid is assumed. On top
of that, the following packages are required:

* `yaml-cpp` in version >= 0.6 (e.g. through Debian package `libyaml-cpp-dev`)

# Configuration

You can see a full working example in `src/example.cc`. You need to run it
with the configuration file as the second argument:

```
./example example.yml
```

The configuration file configures the grid, material properties and fibers.

# Possible extensions

The code assumes 2D and a P2 discretization on triangles. Changing these
assumptions is a major undertaking (that is currently done by the original
authors of the paper).

Currently only straight fibers (specified by start and end point) are considered.
This can however be extended by:

* Providing a new implementation of `FibreParametrizationBase` from `dune/fiber-elasticity/parametrizedcurves.hh`
* Changing the construction mechanism in the constructor of `EulerBernoulli2DLocalOperator` in `dune/fiber-elasticity/eulerbernoulli.hh` to also take into account the new fiber type

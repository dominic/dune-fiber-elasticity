# This file can be used to perform build system checks for
# this Dune module. These checks will be performed upon configuration
# of this module, as well as upon configuration of any dependent module.

find_package(yaml-cpp 0.6 REQUIRED)
dune_register_package_flags(LIBRARIES yaml-cpp)

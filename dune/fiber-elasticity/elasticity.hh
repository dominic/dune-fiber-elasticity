#ifndef ELASTICITY_2D_P2_OPERATOR_HH
#define ELASTICITY_2D_P2_OPERATOR_HH

// DISCLAIMER: This is a generated file!

#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/fiber-elasticity/localbasiscache.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/fiber-elasticity/material.hh"
#include "dune/fiber-elasticity/virtualinterface.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV, typename GFS_COEFF_37, typename GFS_COEFF_38>
class Elasticity2DP2Operator
    : public Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>
{  

  public:
  Elasticity2DP2Operator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams, std::shared_ptr<ElasticMaterialBase<typename GFSU::Traits::EntitySet, double>> material) :
      _iniParams(iniParams),
      material(material)
  {
    fillQuadraturePointsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qp_dim1_order2);
    fillQuadratureWeightsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 2, qw_dim1_order2);
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qp_dim2_order2);
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 2, qw_dim2_order2);
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 1>::Vector> qp_dim1_order2;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 1>::Field> qw_dim1_order2;
  

  public:
  using GFSV_0 = Dune::TypeTree::Child<GFSV,0>;
  

  public:
  using P2_LocalBasis = typename GFSV_0::Traits::FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
  

  public:
  LocalBasisCacheWithoutReferences<P2_LocalBasis> cache_CG2;
  

  public:
  using Coefficient_LFS38 = Dune::PDELab::LocalFunctionSpace<GFS_COEFF_38>;
  

  public:
  mutable std::shared_ptr<Coefficient_LFS38> coefficient_lfs_38;
  

  public:
  using Coefficient_Vector_38 = Dune::PDELab::Backend::Vector<GFS_COEFF_38, double>;
  

  public:
  mutable std::shared_ptr<Coefficient_Vector_38> coefficient_vector_38;
  

  public:
  mutable std::shared_ptr<const GFS_COEFF_38> coefficient_gfs_38;
  

  public:
  using Coefficient_LFS_Cache38 = Dune::PDELab::LFSIndexCache<Coefficient_LFS38>;
  

  public:
  mutable std::shared_ptr<Coefficient_LFS_Cache38> coefficient_lfs_cache_38;
  

  public:
  void setCoefficientTraction(std::shared_ptr<const GFS_COEFF_38> p_gfs, std::shared_ptr<Coefficient_Vector_38> p_coefficient_vector){
    coefficient_gfs_38 = p_gfs;
    coefficient_vector_38 = p_coefficient_vector;
    coefficient_lfs_38 = std::make_shared<Coefficient_LFS38>(*coefficient_gfs_38);
    coefficient_lfs_cache_38 = std::make_shared<Coefficient_LFS_Cache38>(*coefficient_lfs_38);
  }
  

  public:
  using GFSV_1 = Dune::TypeTree::Child<GFSV,1>;
  

  public:
  using IG = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::IG;
  

  public:
  using LFSU = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::LFSU;
  

  public:
  using X = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::X;
  

  public:
  using LFSV = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::LFSV;
  

  public:
  using R = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::R;
  

  public:
  enum { doPatternBoundary = true };
  

  public:
  enum { doAlphaBoundary = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order2;
  

  public:
  using Coefficient_LFS37 = Dune::PDELab::LocalFunctionSpace<GFS_COEFF_37>;
  

  public:
  mutable std::shared_ptr<Coefficient_LFS37> coefficient_lfs_37;
  

  public:
  using Coefficient_Vector_37 = Dune::PDELab::Backend::Vector<GFS_COEFF_37, double>;
  

  public:
  mutable std::shared_ptr<Coefficient_Vector_37> coefficient_vector_37;
  

  public:
  mutable std::shared_ptr<const GFS_COEFF_37> coefficient_gfs_37;
  

  public:
  using Coefficient_LFS_Cache37 = Dune::PDELab::LFSIndexCache<Coefficient_LFS37>;
  

  public:
  mutable std::shared_ptr<Coefficient_LFS_Cache37> coefficient_lfs_cache_37;
  

  public:
  void setCoefficientForce(std::shared_ptr<const GFS_COEFF_37> p_gfs, std::shared_ptr<Coefficient_Vector_37> p_coefficient_vector){
    coefficient_gfs_37 = p_gfs;
    coefficient_vector_37 = p_coefficient_vector;
    coefficient_lfs_37 = std::make_shared<Coefficient_LFS37>(*coefficient_gfs_37);
    coefficient_lfs_cache_37 = std::make_shared<Coefficient_LFS_Cache37>(*coefficient_lfs_37);
  }
  

  public:
  void setMaterial(std::shared_ptr<ElasticMaterialBase<typename GFSU::Traits::EntitySet, double>> material_)
  {
    material = material_;
  }
  

  public:
  std::shared_ptr<ElasticMaterialBase<typename GFSU::Traits::EntitySet, double>> material;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order2;
  

  public:
  using EG = typename Dune::BlockLab::AbstractLocalOperatorInterface<GFSU, GFSV>::EG;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const override
  {
    auto is_geo = ig.geometry();
    const auto quadrature_rule = quadratureRule(is_geo, 2);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_s_0 = child(lfsv_s, _0);
    auto lfsv_s_0_size = lfsv_s_0.size();
    auto geo_in_inside = ig.geometryInInside();
    const auto& inside_cell = ig.inside();
    coefficient_lfs_38->bind(inside_cell);
    auto& deref_coefficient_lfs_38 = * coefficient_lfs_38;
    typename Coefficient_Vector_38::template LocalView<Coefficient_LFS_Cache38> coefficient_view_38(*coefficient_vector_38);
    coefficient_lfs_cache_38->update();
    Dune::PDELab::LocalVector<double> local_coefficient_vector_38_s(coefficient_lfs_38->size());
    coefficient_view_38.bind(*coefficient_lfs_cache_38);
    coefficient_view_38.read(local_coefficient_vector_38_s);
    coefficient_view_38.unbind();
    auto lfsv_s_1 = child(lfsv_s, _1);
    auto lfsv_s_1_size = lfsv_s_1.size();
    double acc_lfsv_s_0_0_trial_index;
    double acc_lfsv_s_0_0_trial_index_0;
    double coefficientFunction38_0_s;
    double coefficientFunction38_1_s;
    double fdetjac;
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::FunctionReturnType phi_CG2_s;
    Dune::FieldVector<double, 2> qp_dim1_order2_in_inside(0.0);
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      acc_lfsv_s_0_0_trial_index_0 = 0.0;
      acc_lfsv_s_0_0_trial_index = 0.0;
      qp_dim1_order2_in_inside = geo_in_inside.global(qp_dim1_order2[q]);
      phi_CG2_s = cache_CG2.evaluateFunction(qp_dim1_order2_in_inside, lfsv_s_0.finiteElement().localBasis());
      fdetjac = is_geo.integrationElement(qp_dim1_order2[q]);
      for (int lfsv_s_0_0_trial_index = 0; lfsv_s_0_0_trial_index <= -1 + lfsv_s_0_size; ++lfsv_s_0_0_trial_index)
      {
        acc_lfsv_s_0_0_trial_index_0 = local_coefficient_vector_38_s(lfsv_s_1, lfsv_s_0_0_trial_index) * (phi_CG2_s[lfsv_s_0_0_trial_index])[0] + acc_lfsv_s_0_0_trial_index_0;
        acc_lfsv_s_0_0_trial_index = local_coefficient_vector_38_s(lfsv_s_0, lfsv_s_0_0_trial_index) * (phi_CG2_s[lfsv_s_0_0_trial_index])[0] + acc_lfsv_s_0_0_trial_index;
      }
      coefficientFunction38_1_s = acc_lfsv_s_0_0_trial_index_0;
      coefficientFunction38_0_s = acc_lfsv_s_0_0_trial_index;
      for (int lfsv_s_0_0_index = 0; lfsv_s_0_0_index <= -1 + lfsv_s_0_size; ++lfsv_s_0_0_index)
      {
        r_s.accumulate(lfsv_s_1, lfsv_s_0_0_index, -1.0 * fdetjac * qw_dim1_order2[q] * (phi_CG2_s[lfsv_s_0_0_index])[0] * coefficientFunction38_1_s);
        r_s.accumulate(lfsv_s_0, lfsv_s_0_0_index, -1.0 * fdetjac * qw_dim1_order2[q] * (phi_CG2_s[lfsv_s_0_0_index])[0] * coefficientFunction38_0_s);
      }
    }
  }
  

  public:
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const override
  {
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_size = lfsv_0.size();
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 2);
    auto quadrature_size = quadrature_rule.size();
    coefficient_lfs_37->bind(eg.entity());
    auto& deref_coefficient_lfs_37 = * coefficient_lfs_37;
    typename Coefficient_Vector_37::template LocalView<Coefficient_LFS_Cache37> coefficient_view_37(*coefficient_vector_37);
    coefficient_lfs_cache_37->update();
    Dune::PDELab::LocalVector<double> local_coefficient_vector_37(coefficient_lfs_37->size());
    coefficient_view_37.bind(*coefficient_lfs_cache_37);
    coefficient_view_37.read(local_coefficient_vector_37);
    coefficient_view_37.unbind();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    double acc_lfsv_0_0_trial_index;
    double acc_lfsv_0_0_trial_index_0;
    double acc_lfsv_0_0_trialgrad_index;
    double acc_lfsv_0_0_trialgrad_index_0;
    double coefficientFunction37_0;
    double coefficientFunction37_1;
    double detjac;
    double gradu_0[2];
    double gradu_1[2];
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::JacobianReturnType js_CG2;
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::FunctionReturnType phi_CG2;
    Dune::FieldMatrix<double, 2, 2> prestress_eval(0.0);
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order2[q]);
      material->prestress(eg.entity(), qp_dim2_order2[q], prestress_eval);
      js_CG2 = cache_CG2.evaluateJacobian(qp_dim2_order2[q], lfsv_0.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order2[q]);
      acc_lfsv_0_0_trial_index_0 = 0.0;
      acc_lfsv_0_0_trial_index = 0.0;
      phi_CG2 = cache_CG2.evaluateFunction(qp_dim2_order2[q], lfsv_0.finiteElement().localBasis());
      for (int lfsv_0_0_trial_index = 0; lfsv_0_0_trial_index <= -1 + lfsv_0_size; ++lfsv_0_0_trial_index)
      {
        acc_lfsv_0_0_trial_index_0 = local_coefficient_vector_37(lfsv_1, lfsv_0_0_trial_index) * (phi_CG2[lfsv_0_0_trial_index])[0] + acc_lfsv_0_0_trial_index_0;
        acc_lfsv_0_0_trial_index = local_coefficient_vector_37(lfsv_0, lfsv_0_0_trial_index) * (phi_CG2[lfsv_0_0_trial_index])[0] + acc_lfsv_0_0_trial_index;
      }
      coefficientFunction37_1 = acc_lfsv_0_0_trial_index_0;
      coefficientFunction37_0 = acc_lfsv_0_0_trial_index;
      for (int idim0 = 0; idim0 <= 1; ++idim0)
      {
        acc_lfsv_0_0_trialgrad_index_0 = 0.0;
        acc_lfsv_0_0_trialgrad_index = 0.0;
        for (int lfsv_0_0_trialgrad_index = 0; lfsv_0_0_trialgrad_index <= -1 + lfsv_0_size; ++lfsv_0_0_trialgrad_index)
        {
          acc_lfsv_0_0_trialgrad_index_0 = x(lfsv_1, lfsv_0_0_trialgrad_index) * ((js_CG2[lfsv_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_trialgrad_index_0;
          acc_lfsv_0_0_trialgrad_index = x(lfsv_0, lfsv_0_0_trialgrad_index) * ((js_CG2[lfsv_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_trialgrad_index;
        }
        gradu_1[idim0] = acc_lfsv_0_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_0_0_trialgrad_index;
      }
      for (int lfsv_0_0_index = 0; lfsv_0_0_index <= -1 + lfsv_0_size; ++lfsv_0_0_index)
      {
        r.accumulate(lfsv_1, lfsv_0_0_index, -1.0 * qw_dim2_order2[q] * detjac * (phi_CG2[lfsv_0_0_index])[0] * coefficientFunction37_1);
        r.accumulate(lfsv_0, lfsv_0_0_index, -1.0 * qw_dim2_order2[q] * detjac * (phi_CG2[lfsv_0_0_index])[0] * coefficientFunction37_0);
        if (material->material_law_index(eg.entity()) == 0)
        {
          r.accumulate(lfsv_1, lfsv_0_0_index, (-1.0 * (phi_CG2[lfsv_0_0_index])[0] * coefficientFunction37_1 + ((jit[0])[0] * ((js_CG2[lfsv_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG2[lfsv_0_0_index])[0])[1]) * ((0.5 * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1]) + 0.5 * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1])) * material->parameter_unrolled(eg.entity(), 0, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1]) + (prestress_eval[1])[0]) + ((jit[1])[0] * ((js_CG2[lfsv_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG2[lfsv_0_0_index])[0])[1]) * ((prestress_eval[1])[1] + (0.5 * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1]) + 0.5 * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1])) * material->parameter_unrolled(eg.entity(), 1, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1]) + (0.5 * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) + 0.5 * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1])) * material->parameter_unrolled(eg.entity(), 0, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1]))) * qw_dim2_order2[q] * detjac);
          r.accumulate(lfsv_0, lfsv_0_0_index, (-1.0 * (phi_CG2[lfsv_0_0_index])[0] * coefficientFunction37_0 + ((jit[0])[0] * ((js_CG2[lfsv_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG2[lfsv_0_0_index])[0])[1]) * ((prestress_eval[0])[0] + (0.5 * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1]) + 0.5 * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1])) * material->parameter_unrolled(eg.entity(), 1, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1]) + (0.5 * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1]) + 0.5 * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1])) * material->parameter_unrolled(eg.entity(), 0, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1])) + ((jit[1])[0] * ((js_CG2[lfsv_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG2[lfsv_0_0_index])[0])[1]) * ((0.5 * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1]) + 0.5 * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1])) * material->parameter_unrolled(eg.entity(), 0, (qp_dim2_order2[q])[0], (qp_dim2_order2[q])[1]) + (prestress_eval[0])[1])) * qw_dim2_order2[q] * detjac);
        }
      }
    }
  }
};


#pragma GCC diagnostic pop

#endif //ELASTICITY_2D_P2_OPERATOR_HH

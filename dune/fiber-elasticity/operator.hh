#ifndef DUNE_FIBER_REINFORCED_OPERATOR_HH
#define DUNE_FIBER_REINFORCED_OPERATOR_HH

#include<dune/fiber-elasticity/elasticity.hh>
#include<dune/fiber-elasticity/eulerbernoulli.hh>
#include<dune/fiber-elasticity/virtualinterface.hh>
#include<dune/fiber-elasticity/yaml.hh>

#include<memory>

template<typename GFS, typename FGFS, typename TGFS, int dim>
class FibreReinforcedBulkOperator
  : public Dune::BlockLab::AbstractLocalOperatorInterface<GFS>
{
  public:
  using BaseOperator = Dune::BlockLab::AbstractLocalOperatorInterface<GFS>;
  using BulkOperator = Elasticity2DP2Operator<GFS, GFS, GFS, GFS>;
  using FibreOperator = EulerBernoulli2DLocalOperator<GFS, FGFS>;

  using EG = typename BaseOperator::EG;
  using IG = typename BaseOperator::IG;
  using LFSU = typename BaseOperator::LFSU;
  using LFSV = typename BaseOperator::LFSV;
  using X = typename BaseOperator::X;
  using R = typename BaseOperator::R;

  FibreReinforcedBulkOperator(std::shared_ptr<const GFS> gfs,
                              const YAML::Node& params,
                              std::shared_ptr<ElasticMaterialBase<typename GFS::Traits::EntitySet, double>> material)
    : bulkoperator(*gfs, *gfs, Dune::ParameterTree(), material)
    , fibreoperator(params, gfs)
  {}

  virtual ~FibreReinforcedBulkOperator() = default;

  void compute_grid_intersection()
  {
    fibreoperator.compute_grid_intersection();
  }

  template<typename CGFS, typename CVEC>
  void setCoefficientForce(std::shared_ptr<CGFS> gfs, std::shared_ptr<CVEC> force)
  {
    bulkoperator.setCoefficientForce(gfs, force);
    fibreoperator.setCoefficientForce(gfs, force);
  }

  void setMaterial(std::shared_ptr<ElasticMaterialBase<typename GFS::Traits::EntitySet, double>> material)
  {
    bulkoperator.setMaterial(material);
  }

  template<typename CGFS, typename CVEC>
  void setCoefficientTraction(std::shared_ptr<CGFS> gfs, std::shared_ptr<CVEC> traction)
  {
    bulkoperator.setCoefficientTraction(gfs, traction);
  }

  virtual void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const override
  {
    bulkoperator.alpha_volume(eg, lfsu, x, lfsv, r);
    fibreoperator.alpha_volume(eg, lfsu, x, lfsv, r);
  }

  virtual void alpha_boundary(const IG& ig, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const override
  {
    bulkoperator.alpha_boundary(ig, lfsu, x, lfsv, r);
  }

  virtual void alpha_skeleton(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n, R& r_s, R& r_n) const
  {
    //bulkoperator.alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n);
    fibreoperator.alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n);
  }

  private:
  BulkOperator bulkoperator;
  EulerBernoulli2DLocalOperator<GFS, FGFS> fibreoperator;
};

#endif

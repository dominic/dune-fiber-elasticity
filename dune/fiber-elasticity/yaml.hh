#ifndef DUNE_BLOCKLAB_UTILITIES_YAML_HH
#define DUNE_BLOCKLAB_UTILITIES_YAML_HH

#include<yaml-cpp/yaml.h>

/** @cond YAML_STUFF 
 * 
 * We are omitting these YAML conversion specialization from the Doxygen Docs as they
 * are not all that helpful and they show up quite prominently due them being in a
 * separate namespace.
*/
namespace YAML {

  template<typename T, int n>
  struct convert<Dune::FieldVector<T, n>>
  {
    static Node encode(const Dune::FieldVector<T, n>& rhs)
    {
      Node node;
      for(int i=0; i<n; ++i)
        node.push_back(rhs[i]);
      return node;
    }

    static bool decode(const Node& node, Dune::FieldVector<T, n>& rhs)
    {
      if(!node.IsSequence() || node.size() < n)
        return false;

      for(int i=0; i<n; ++i)
        rhs[i] = node[i].as<T>();
      return true;
    }
  };
}

#endif
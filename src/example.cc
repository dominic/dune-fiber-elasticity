#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// dune-fiber-elasticity specific includes you probably want to add
#include<yaml-cpp/yaml.h>
#include<dune/fiber-elasticity/material.hh>
#include<dune/fiber-elasticity/operator.hh>

// Stuff that I use for setting up the problem
#include<dune/fiber-elasticity/callabletree.hh>
#include<dune/fiber-elasticity/vonmises.hh>

// General Dune stuff
#include<dune/common/parallel/mpihelper.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/grid/uggrid.hh>
#include<dune/pdelab.hh>

#include<memory>
#include<vector>

int main(int argc, char** argv)
{
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

  // Parse configuration  
  YAML::Node config = YAML::LoadFile(argv[1]);
  
  // Construct a 2D UG grid - only sequential here
  using Grid = Dune::UGGrid<2>;
  auto ll = config["grid"]["lowerleft"].template as<Dune::FieldVector<double, 2>>(Dune::FieldVector<double, 2>(0.0));
  auto ur = config["grid"]["upperright"].template as<Dune::FieldVector<double, 2>>(Dune::FieldVector<double, 2>(1.0));
  auto N = config["grid"]["N"].template as<std::array<unsigned int, 2>>(Dune::filledArray<2, unsigned int>(10));
  auto grid = Dune::StructuredGridFactory<Grid>::createSimplexGrid(ll, ur, N);
  grid->loadBalance();
  grid->globalRefine(config["grid"]["refinement"].template as<int>(0));
  auto physical = std::make_shared<std::vector<int>>(grid->size(0), 0);
  auto gv = grid->leafGridView();
  using EntitySet = Dune::PDELab::OverlappingEntitySet<Grid::Traits::LeafGridView>;
  EntitySet es(gv);
  
  // PDELab stuff
  using FiniteElementMap = Dune::PDELab::PkLocalFiniteElementMap<EntitySet, double, double, 2>;
  using ConstraintsAssembler = Dune::PDELab::OverlappingConformingDirichletConstraints;
  using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  using LeafVectorBackend = Dune::PDELab::ISTL::VectorBackend<>;  
  using VectorGridFunctionSpace = Dune::PDELab::VectorGridFunctionSpace<EntitySet, FiniteElementMap, 2, VectorBackend, LeafVectorBackend, ConstraintsAssembler>;
  using ConstraintsContainer = typename VectorGridFunctionSpace::template ConstraintsContainer<double>::Type;
  FiniteElementMap fem(es);
  VectorGridFunctionSpace vgfs(es, fem);
  ConstraintsContainer cc;

  // Parsing the material description
  auto material = parse_material<double>(es, physical, config["material"], config);

  // Interpolation of initial conditions
  using Vector = Dune::PDELab::Backend::Vector<VectorGridFunctionSpace, double>;
  Vector displacement(vgfs, 0.0);
  Vector body_force(vgfs, 0.0);
  Vector traction_force(vgfs, 0.0);

  auto body_force_func = Dune::BlockLab::makeGridFunctionTreeFromCallables(vgfs,
    [](auto, auto){ return 0; }, [](auto, auto){ return -1; });
  Dune::PDELab::interpolate(body_force_func, vgfs, body_force);

  auto cc_func = Dune::BlockLab::makeBoundaryConditionTreeFromCallables(vgfs, 
    [](auto e, auto x){ return e.geometry().global(x)[0] < 1e-8; },
    [](auto e, auto x){ return e.geometry().global(x)[0] < 1e-8; });
  Dune::PDELab::constraints(cc_func, vgfs, cc);

  // Construction of operators
  using LocalOperator = FibreReinforcedBulkOperator<VectorGridFunctionSpace, VectorGridFunctionSpace, VectorGridFunctionSpace, 2>;
  LocalOperator lop(Dune::stackobject_to_shared_ptr(vgfs), config["reinforced_operator"], material);
  Dune::PDELab::ISTL::BCRSMatrixBackend<> mb(36);
  using GridOperator = Dune::PDELab::GridOperator<VectorGridFunctionSpace,
						    VectorGridFunctionSpace,
						    LocalOperator,
						    Dune::PDELab::ISTL::BCRSMatrixBackend<>,
						    double,
						    double,
						    double,
						    ConstraintsContainer,
						    ConstraintsContainer>;
  GridOperator go(vgfs, cc, vgfs, cc, lop, mb);

  // Linear Solver Setup
  using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_UMFPack;
  using StationaryLinearProblemSolver = Dune::PDELab::StationaryLinearProblemSolver<GridOperator, LinearSolver, Vector>;
  LinearSolver ls(0);
  StationaryLinearProblemSolver slp(go, ls, displacement, 1e-12);
  
  // Apply solver
  lop.setCoefficientTraction(Dune::stackobject_to_shared_ptr(vgfs), Dune::stackobject_to_shared_ptr(traction_force));
  lop.setCoefficientForce(Dune::stackobject_to_shared_ptr(vgfs), Dune::stackobject_to_shared_ptr(body_force));
  slp.apply();

  // Visualization
  Dune::SubsamplingVTKWriter vtkwriter(gv, Dune::RefinementIntervals(2));

  // Add the displacement field visualization
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, vgfs, displacement , Dune::PDELab::vtk::DefaultFunctionNameGenerator("Displacement"));

  // Add von Mises stress visualization
  using P1FEM = Dune::PDELab::PkLocalFiniteElementMap<EntitySet, double, double, 1>;
  using P1GFS = Dune::PDELab::GridFunctionSpace<EntitySet, P1FEM, Dune::PDELab::NoConstraints, LeafVectorBackend>;
  using StressVector = Dune::PDELab::Backend::Vector<P1GFS, double>;
  P1FEM p1fem(es);
  P1GFS p1gfs(es, p1fem);
  StressVector stress_container(p1gfs);
  VonMisesStressGridFunction<Vector, 2> stress(displacement, material);
  Dune::PDELab::interpolate(stress, p1gfs, stress_container);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, p1gfs, stress_container, Dune::PDELab::vtk::DefaultFunctionNameGenerator("von-Mises stress"));

  // Write visualization to file
  vtkwriter.write("output", Dune::VTK::appendedraw);

  return 0;
}

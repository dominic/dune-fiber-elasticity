#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// dune-fiber-elasticity specific includes you probably want to add
#include<yaml-cpp/yaml.h>
#include<dune/fiber-elasticity/material.hh>
#include<dune/fiber-elasticity/operator.hh>

// Stuff that I use for setting up the problem
#include<dune/fiber-elasticity/callabletree.hh>
#include<dune/fiber-elasticity/vonmises.hh>

// General Dune stuff
#include<dune/common/parametertreeparser.hh>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/grid/uggrid.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/pdelab.hh>

#include<memory>
#include<vector>

// ftworth includes
#include<dune/ftworth/partitioner.hh>
#include<dune/ftworth/assemblewrapper.hh>
#include<dune/ftworth/subdomainutilities.hh>
#include<dune/ftworth/cdediscretizer.hh>
#include<dune/ftworth/multilevel_geneo_preconditioner.hh>

int main(int argc, char** argv)
{
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

  // Parse configuration
  if (argc==1)
    {
      std::cout << "./example <yml file>" << std::endl;
      exit(1);
    }
  YAML::Node config = YAML::LoadFile(argv[1]);
  
  // Construct a 2D UG grid - only sequential here
  using Grid = Dune::UGGrid<2>;
  auto ll = config["grid"]["lowerleft"].template as<Dune::FieldVector<double, 2>>(Dune::FieldVector<double, 2>(0.0));
  auto ur = config["grid"]["upperright"].template as<Dune::FieldVector<double, 2>>(Dune::FieldVector<double, 2>(1.0));
  auto N = config["grid"]["N"].template as<std::array<unsigned int, 2>>(Dune::filledArray<2, unsigned int>(10));
  auto grid = Dune::StructuredGridFactory<Grid>::createSimplexGrid(ll, ur, N);
  grid->loadBalance();
  grid->globalRefine(config["grid"]["refinement"].template as<int>(0));
  auto physical = std::make_shared<std::vector<int>>(grid->size(0), 0);
  auto gv = grid->leafGridView();
  using EntitySet = Dune::PDELab::OverlappingEntitySet<Grid::Traits::LeafGridView>;
  EntitySet es(gv);

  // PDELab stuff
  using FiniteElementMap = Dune::PDELab::PkLocalFiniteElementMap<EntitySet, double, double, 2>;
  using ConstraintsAssembler = Dune::PDELab::OverlappingConformingDirichletConstraints;
  using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  using LeafVectorBackend = Dune::PDELab::ISTL::VectorBackend<>;  
  using VectorGridFunctionSpace = Dune::PDELab::VectorGridFunctionSpace<EntitySet, FiniteElementMap, 2, VectorBackend, LeafVectorBackend, ConstraintsAssembler>;
  using ConstraintsContainer = typename VectorGridFunctionSpace::template ConstraintsContainer<double>::Type;
  FiniteElementMap fem(es);
  VectorGridFunctionSpace vgfs(es, fem);
  ConstraintsContainer cc;

  // Parsing the material description
  auto material = parse_material<double>(es, physical, config["material"], config);

  // Interpolation of initial conditions
  using Vector = Dune::PDELab::Backend::Vector<VectorGridFunctionSpace, double>;
  Vector displacement(vgfs, 0.0);
  Vector body_force(vgfs, 0.0);
  Vector traction_force(vgfs, 0.0);

  auto body_force_func = Dune::BlockLab::makeGridFunctionTreeFromCallables(vgfs,
    [](auto, auto){ return 0; }, [](auto, auto){ return -1; });
  Dune::PDELab::interpolate(body_force_func, vgfs, body_force);

  auto cc_func = Dune::BlockLab::makeBoundaryConditionTreeFromCallables(vgfs, 
    [](auto e, auto x){ return e.geometry().global(x)[0] < 1e-8; },
    [](auto e, auto x){ return e.geometry().global(x)[0] < 1e-8; });
  Dune::PDELab::constraints(cc_func, vgfs, cc);
  std::cout << "constrained dofs=" << cc.size() << " of " << vgfs.globalSize() << std::endl;

  // Construction of operators
  using LocalOperator = FibreReinforcedBulkOperator<VectorGridFunctionSpace, VectorGridFunctionSpace, VectorGridFunctionSpace, 2>;
  LocalOperator lop(Dune::stackobject_to_shared_ptr(vgfs), config["reinforced_operator"], material);
  Dune::PDELab::ISTL::BCRSMatrixBackend<> mb(36);
  using GridOperator = Dune::PDELab::GridOperator<VectorGridFunctionSpace,
						    VectorGridFunctionSpace,
						    LocalOperator,
						    Dune::PDELab::ISTL::BCRSMatrixBackend<>,
						    double,
						    double,
						    double,
						    ConstraintsContainer,
						    ConstraintsContainer>;
  GridOperator go(vgfs, cc, vgfs, cc, lop, mb);
  lop.setCoefficientTraction(Dune::stackobject_to_shared_ptr(vgfs), Dune::stackobject_to_shared_ptr(traction_force));
  lop.setCoefficientForce(Dune::stackobject_to_shared_ptr(vgfs), Dune::stackobject_to_shared_ptr(body_force));

  //==================
  // Begin Geneo stuff
  //==================
  
  // read parameters
  Dune::ParameterTree ptree;
  Dune::ParameterTreeParser ptreeparser;
  ptreeparser.readINITree("example.ini",ptree);
  ptreeparser.readOptions(argc,argv,ptree);
  std::vector<size_t> subdomains = ptree.get<std::vector<size_t>>("geneo.subdomains");
  size_t overlap = ptree.get("geneo.overlap",(size_t)1);
  std::string extensionmethod = ptree.get("geneo.extensionmethod","vertex");
  size_t drop_small_overlap = ptree.get("geneo.drop_small_overlap",(size_t)1);
  bool coordinate_partitioning = ptree.get("geneo.coordinate_partitioning",(bool)false);
  std::vector<size_t> parts = ptree.get<std::vector<size_t>>("geneo.parts");

  // load balancing and subdomain information for geneo
  std::shared_ptr<DomainDecomposition> pdd;
  if (coordinate_partitioning)
    pdd = std::make_shared<DomainDecomposition>(gv,subdomains[0],parts,overlap,extensionmethod,drop_small_overlap,false);
  else
    pdd = std::make_shared<DomainDecomposition>(gv,Dune::MPIHelper::getLocalCommunicator(),subdomains[0],overlap,extensionmethod,drop_small_overlap,false);
  auto pdddm = std::make_shared<DomainDecompositionDOFMapper>(vgfs,pdd);

  // set up linear system in PDELab components
  using Matrix = typename GridOperator::Jacobian;
  using ISTLV = Dune::PDELab::Backend::Native<Vector>;
  using ISTLM = Dune::PDELab::Backend::Native<Matrix>;
  ISTLV& istldisplacement = Dune::PDELab::Backend::native(displacement);
  Matrix A(go); // this makes the sparsity pattern
  A = 0.0; // clear matrix
  ISTLM& istlA = Dune::PDELab::Backend::native(A);
  std::shared_ptr<ISTLM> pistlA = stackobject_to_shared_ptr(Dune::PDELab::Backend::native(istlA));

  // residual
  Vector residual(vgfs);
  residual = 0.0;
  go.residual(displacement,residual);
  ISTLV& istlr = Dune::PDELab::Backend::native(residual);

  // create snippet matrices
  std::cout << "create sparse matrices for snippets and subdomains" << std::endl;
  auto pvolume_snippet_matrices = std::shared_ptr<std::vector<std::shared_ptr<ISTLM>>>( new std::vector<std::shared_ptr<ISTLM>>(set_up_local_matrices(istlA,pdddm->volumesnippetnumber_local_to_global,pdddm->volumesnippetnumber_global_to_local)) );
  for (auto matptr : *pvolume_snippet_matrices) *matptr = 0.0; // clear subdomain matrices
  std::shared_ptr<std::vector<std::shared_ptr<ISTLM>>> pskeleton_snippet_matrices; // the empty pointer
  pskeleton_snippet_matrices = std::shared_ptr<std::vector<std::shared_ptr<ISTLM>>>( new std::vector<std::shared_ptr<ISTLM>>(set_up_local_matrices(istlA,pdddm->skeletonsnippetnumber_local_to_global,pdddm->skeletonsnippetnumber_global_to_local)) );
  for (auto matptr : *pskeleton_snippet_matrices) *matptr = 0.0; // clear subdomain matrices

  // make wrapped local operator
  std::cout << "assembling subdomain matrices" << std::endl;
  using WLOP = SnippetAssemblerWrapper<LocalOperator,ISTLM,VectorGridFunctionSpace>;
  WLOP wlop(vgfs,lop,pdddm,pvolume_snippet_matrices,pskeleton_snippet_matrices);
  using WGOP = Dune::PDELab::GridOperator<VectorGridFunctionSpace,VectorGridFunctionSpace,WLOP,
					  Dune::PDELab::ISTL::BCRSMatrixBackend<>,double,double,double,
					  ConstraintsContainer,ConstraintsContainer>;
  WGOP wgop(vgfs,cc,vgfs,cc,wlop,mb);
  Dune::Timer timer;
  timer.reset();
  wgop.jacobian(displacement,A); // assembles the global stiffness matrix and the snippets
  auto assemble_time = timer.elapsed();
  std::cout << "SNIPPET ASSEMBLE TIME=" << assemble_time << std::endl;
  //Dune::printmatrix(std::cout, istlA, " A", "");

  // global vector with dirichlet boundary conditions
  // contains 0.0 for dof on Dirichlet boundary, otherwise 1.0
  Vector dirichlet_mask(vgfs);
  dirichlet_mask = 1.0;
  set_constrained_dofs(cc,0.0,dirichlet_mask); // dirichlet dofs are zero, others are 1, so we can use it as a mask!
  std::shared_ptr<ISTLV> pistldirichlet_mask = stackobject_to_shared_ptr(Dune::PDELab::Backend::native(dirichlet_mask));

  // symmetric elimination of Dirichlet boundary conditions; so we can check symmetry
  auto blocksize = (*pistldirichlet_mask)[0].size();
  for (size_t i=0; i<istlA.N(); i++)
    for (size_t compi=0; compi<blocksize; compi++)
      if ((*pistldirichlet_mask)[i][compi]==1.0) // this dof is NOT on the Dirichlet boundary
	{
	  // non dirchlet row; eliminate Dirichlet columns
	  auto cIt = istlA[i].begin();
	  auto cEndIt = istlA[i].end();
	  for (; cIt!=cEndIt; ++cIt)
	    for (size_t compj=0; compj<blocksize; compj++)
	      if ((*pistldirichlet_mask)[cIt.index()][compj]==0.0)
		  (*cIt)[compi][compj] = 0.0;
	}
  bool sym = is_symmetric(istlA,"A",false,1e-8);
  std::cout << "matrix is symmetric: " << sym << std::endl;
  std::cout << "matrix norm: " << istlA.infinity_norm() << std::endl;
  // for (size_t i=0; i<istlA.N(); i++)
  //   {
  //     auto cIt = istlA[i].begin();
  //     auto cEndIt = istlA[i].end();
  //     for (; cIt!=cEndIt; ++cIt)
  // 	if (cIt.index()==i)
  // 	  for (size_t compi=0; compi<blocksize; compi++)
  // 	    std::cout << i << " " << compi << " " << (*cIt)[compi][compi] << std::endl;
  //   }

  size_t coarse_overlap = ptree.get("geneo.coarse_overlap",(size_t)0);
  std::string pum = ptree.get("geneo.pum","standard");
  std::string fineGEVPrhs = ptree.get("geneo.fineGEVPrhs","pu");
  std::string coarseGEVPrhs = ptree.get("geneo.coarseGEVPrhs","pu");
  std::string coarseeigensolver = ptree.get("geneo.coarseeigensolver","eigen");
  std::string cycle = ptree.get("geneo.cycle","additive");
  size_t n_eigenvectors_fine_computed = ptree.get("geneo.n_eigenvectors_fine_computed",(size_t)10);
  size_t n_eigenvectors_fine_used = ptree.get("geneo.n_eigenvectors_fine_used",(size_t)5);
  size_t n_eigenvectors_coarse_computed = ptree.get("geneo.n_eigenvectors_coarse_computed",(size_t)10);
  size_t n_eigenvectors_coarse_used = ptree.get("geneo.n_eigenvectors_coarse_used",(size_t)5);
  size_t mlgeneo_verbose = ptree.get("geneo.verbose",(size_t)0);
  double abs_zero_ker = ptree.get("geneo.abs_zero_ker",(double)1e-10);
  double regularization_ker = ptree.get("geneo.regularization_ker",(double)0.0);
  double eigenvalue_fine_threshold = ptree.get("geneo.eigenvalue_fine_threshold",(double)-1.0);
  double eigenvalue_coarse_threshold = ptree.get("geneo.eigenvalue_coarse_threshold",(double)-1.0);
  double arpack_tolerance = ptree.get("geneo.arpack_tolerance",(double)0.0);
  bool merge_disconnected = ptree.get("geneo.merge_disconnected",(bool)false);

  std::string solvertype = ptree.get("solver.type","direct");

  if (solvertype=="geneo")
    {
      timer.reset();
      MultiLevelGenEO<ISTLM,ISTLV> precx(Dune::MPIHelper::getLocalCommunicator(),
				    pistlA,
				    pistldirichlet_mask,
				    pvolume_snippet_matrices,
				    pskeleton_snippet_matrices,
				    pdddm,
				    subdomains,
				    pum,
				    fineGEVPrhs,
				    coarseGEVPrhs,
				    coarse_overlap,
				    coarseeigensolver,
				    arpack_tolerance,
				    n_eigenvectors_fine_computed,n_eigenvectors_fine_used,
				    n_eigenvectors_coarse_computed,n_eigenvectors_coarse_used,
				    eigenvalue_fine_threshold,
				    eigenvalue_coarse_threshold,
				    abs_zero_ker,
				    regularization_ker,
				    merge_disconnected,
				    cycle,
				    mlgeneo_verbose);
      auto setup_time = timer.elapsed();
      std::cout << "SETUP TIME=" << setup_time << std::endl;

      // set up ISTL solver
      size_t itmax = ptree.get("solver.itmax",(size_t)100);
      double red = ptree.get("solver.reduction",(double)1e-6);
      using Operator = Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV>;
      Operator op(istlA);
      //Dune::SeqILU<ISTLM,ISTLV,ISTLV> prec(istlA,0,1.0,false);
      Dune::Richardson<ISTLV,ISTLV> prec(1.0);
      Dune::CGSolver<ISTLV> solver(op,precx,red,itmax,2);
      //Dune::RestartedGMResSolver<ISTLV> solver(op,precx,red,50,itmax,2);
      Dune::InverseOperatorResult stat;
      Vector v(vgfs);
      ISTLV& istlv = Dune::PDELab::Backend::native(v);
      v = 1.0;
      set_constrained_dofs(cc,0.0,v); // dirichlet dofs are zero, others are 1, so we can use it as a mask!
      solver.apply(istlv,istlr,stat);
      istldisplacement -= istlv;
    }

  //==================
  // The old solver
  //==================
  
  // The old solver
  if (solvertype=="direct")
    {
      using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_UMFPack;
      using StationaryLinearProblemSolver = Dune::PDELab::StationaryLinearProblemSolver<GridOperator, LinearSolver, Vector>;
      LinearSolver ls(0);
      StationaryLinearProblemSolver slp(go, ls, displacement, 1e-12);
      slp.apply();
    }
  
  // Visualization
  Dune::SubsamplingVTKWriter vtkwriter(gv, Dune::RefinementIntervals(2));
  //Dune::VTKWriter vtkwriter(gv);

  // Add the displacement field visualization
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, vgfs, displacement , Dune::PDELab::vtk::DefaultFunctionNameGenerator("Displacement"));

  // Add von Mises stress visualization
  using P1FEM = Dune::PDELab::PkLocalFiniteElementMap<EntitySet, double, double, 1>;
  using P1GFS = Dune::PDELab::GridFunctionSpace<EntitySet, P1FEM, Dune::PDELab::NoConstraints, LeafVectorBackend>;
  using StressVector = Dune::PDELab::Backend::Vector<P1GFS, double>;
  P1FEM p1fem(es);
  P1GFS p1gfs(es, p1fem);
  StressVector stress_container(p1gfs);
  VonMisesStressGridFunction<Vector, 2> stress(displacement, material);
  Dune::PDELab::interpolate(stress, p1gfs, stress_container);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, p1gfs, stress_container, Dune::PDELab::vtk::DefaultFunctionNameGenerator("von-Mises stress"));
  // vtkwriter.addCellData(pdd->partition,"partition 0");
  // vtkwriter.addCellData(pdd->vizpartition,"vizpartition 0");
  // std::vector<unsigned> k(gv.size(0));
  // for (size_t i=0; i<gv.size(0); ++i)
  //   k[i] = pdd->overlappingsubdomains[i].size();
  // vtkwriter.addCellData(k,"k_0");

  // Write visualization to file
  vtkwriter.write("output", Dune::VTK::appendedraw);

  return 0;
}
